/*
 * See LICENSE for details
 */
#include <stdlib.h>
#include <string.h>

#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <unibilium.h>

#define WRAP_UNIBI_NAME "unibilium.unibi*"
#define WRAP_UNIBICMD_NAME "unibilium.str_property*"

#if defined (LUA_VERSION_NUM) && \
        LUA_VERSION_NUM <= 502
static int
lua_isinteger(lua_State *L, int i)
{
        lua_Number n;
        lua_Integer d;

        if (lua_type(L, i) != LUA_TNUMBER) {
                return 0;
        }

        n = lua_tonumber(L, i);
        d = lua_tointeger(L, i);

        return n == d;
}

#endif

#if defined (LUA_VERSION_NUM) && \
        LUA_VERSION_NUM <= 501
static void
lua_setuservalue(lua_State *L, lua_Integer x)
{
        luaL_checktype(L, -1, LUA_TTABLE);
        lua_setfenv(L, x);
}

#endif

static unibi_var_t ZERO;

int
wrap_unibicmd_call(lua_State *L)
{
        char **cmd = (char **) luaL_checkudata(L, 1, WRAP_UNIBICMD_NAME);
        unibi_var_t p[9] = { ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO,
                             ZERO };
        char *tmp[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        char buf[256] = { 0 };
        char *large_buf = 0;
        size_t n = 0;

        for (long j = 1; j <= 9; ++j) {
                if (lua_isinteger(L, j + 1)) {
                        p[j - 1] = unibi_var_from_num(lua_tointeger(L, j + 1));
                } else if (lua_isstring(L, j + 1)) {
                        size_t from_lua_len = 0;
                        const char *from_lua = lua_tolstring(L, j + 1,
                                                             &from_lua_len);

                        if ((tmp[j - 1] = malloc(from_lua_len + 1))) {
                                strncpy(tmp[j - 1], from_lua, from_lua_len);
                                tmp[j - 1][from_lua_len] = 0;
                                p[j - 1] = unibi_var_from_str(tmp[j - 1]);
                        }
                }
        }

        n = unibi_run(*cmd, p, buf, sizeof(buf) - 1);

        if (n < sizeof(buf) - 1) {
                buf[n] = 0;
                lua_pushstring(L, buf);
        } else {
                if (!(large_buf = malloc(n + 1))) {
                        lua_pushnil(L);
                } else {
                        unibi_run(*cmd, p, large_buf, n);
                        large_buf[n] = 0;
                        lua_pushstring(L, large_buf);
                        free(large_buf);
                }
        }

        for (size_t j = 0; j < 9; ++j) {
                free(tmp[j]);
        }

        return 1;
}

int
wrap_unibicmd_gc(lua_State *L)
{
        char **cmd = (char **) luaL_checkudata(L, 1, WRAP_UNIBICMD_NAME);

        free(*cmd);
        *cmd = 0;

        return 0;
}

static int
make_unibi_cmd_object(lua_State *L, const char *cmd_from_unibi)
{
        size_t cmd_from_unibi_len = 0;
        char **cmd = 0;

        if (!cmd_from_unibi) {
                lua_pushnil(L);

                return 1;
        }

        cmd_from_unibi_len = strlen(cmd_from_unibi);
        cmd = (char **) lua_newuserdata(L, sizeof *cmd);
        luaL_getmetatable(L, WRAP_UNIBICMD_NAME);
        lua_pushvalue(L, -1);
        lua_setuservalue(L, -3);
        lua_setmetatable(L, -2);

        if (!(*cmd = malloc(cmd_from_unibi_len + 1))) {
                lua_pop(L, 1);
                lua_pushnil(L);

                return 1;
        }

        strncpy(*cmd, cmd_from_unibi, cmd_from_unibi_len + 1);
        (*cmd)[cmd_from_unibi_len] = 0;

        return 1;
}

static const struct luaL_Reg wrap_unibicmd_meta[] = {
        /* */
        { "__call", wrap_unibicmd_call }, /* */
        { "__gc", wrap_unibicmd_gc },     /* */
        { 0 },                            /* */
};

int
wrap_unibi_index(lua_State *L)
{
        unibi_term **ut = (unibi_term **) luaL_checkudata(L, 1,
                                                          WRAP_UNIBI_NAME);
        const char *key = 0;
        const char *test = 0;
        enum unibi_boolean ubool;
        enum unibi_numeric unum;
        enum unibi_string ustring;
        size_t ext_num = 0;

        if (!(lua_isstring(L, 2))) {
                lua_pushnil(L);

                return 1;
        }

        key = lua_tolstring(L, 2, 0);

        /*
           This is probably very, very slow. A clever implementation
           would use some kind of hash table to pre-compute
           string-to-enum mappings.
         */

        /* Test booleans */
        for (ubool = unibi_boolean_begin_ + 1; ubool < unibi_boolean_end_;
             ++ubool) {
                test = unibi_name_bool(ubool);

                if (!strcmp(key, test)) {
                        lua_pushboolean(L, unibi_get_bool(*ut, ubool));

                        return 1;
                }

                test = unibi_short_name_bool(ubool);

                if (!strcmp(key, test)) {
                        lua_pushboolean(L, unibi_get_bool(*ut, ubool));

                        return 1;
                }
        }

        ext_num = unibi_count_ext_bool(*ut);

        for (size_t j = 0; j < ext_num; ++j) {
                test = unibi_get_ext_bool_name(*ut, j);

                if (!strcmp(key, test)) {
                        lua_pushboolean(L, unibi_get_ext_bool(*ut, j));

                        return 1;
                }
        }

        /* Test numbers */
        for (unum = unibi_numeric_begin_ + 1; unum < unibi_numeric_end_;
             ++unum) {
                test = unibi_name_num(unum);

                if (!strcmp(key, test)) {
                        lua_pushinteger(L, unibi_get_num(*ut, unum));

                        return 1;
                }

                test = unibi_short_name_num(unum);

                if (!strcmp(key, test)) {
                        lua_pushinteger(L, unibi_get_num(*ut, unum));

                        return 1;
                }
        }

        ext_num = unibi_count_ext_num(*ut);

        for (size_t j = 0; j < ext_num; ++j) {
                test = unibi_get_ext_num_name(*ut, j);

                if (!strcmp(key, test)) {
                        lua_pushinteger(L, unibi_get_ext_num(*ut, j));

                        return 1;
                }
        }

        /* Test strings */
        for (ustring = unibi_string_begin_ + 1; ustring < unibi_string_end_;
             ++ustring) {
                test = unibi_name_str(ustring);

                if (!strcmp(key, test)) {
                        return make_unibi_cmd_object(L, unibi_get_str(*ut,
                                                                      ustring));
                }

                test = unibi_short_name_str(ustring);

                if (!strcmp(key, test)) {
                        return make_unibi_cmd_object(L, unibi_get_str(*ut,
                                                                      ustring));
                }
        }

        ext_num = unibi_count_ext_str(*ut);

        for (size_t j = 0; j < ext_num; ++j) {
                test = unibi_get_ext_str_name(*ut, j);

                if (!strcmp(key, test)) {
                        return make_unibi_cmd_object(L, unibi_get_ext_str(*ut,
                                                                          j));
                }
        }

        lua_pushnil(L);

        return 1;
}

int
wrap_unibi_gc(lua_State *L)
{
        unibi_term **ut = (unibi_term **) luaL_checkudata(L, 1,
                                                          WRAP_UNIBI_NAME);

        if (*ut) {
                unibi_destroy(*ut);
        }

        return 0;
}

static const struct luaL_Reg wrap_unibi_meta[] = {
        /* */
        { "__index", wrap_unibi_index }, /* */
        { "__gc", wrap_unibi_gc },       /* */
        { 0 },                           /* */
};

static int
wrap_from_mem(lua_State *L)
{
        unibi_term **ut = (unibi_term **) lua_newuserdata(L, sizeof *ut);
        const char *mem_arg = 0;
        size_t mem_len = 0;

        luaL_getmetatable(L, WRAP_UNIBI_NAME);
        lua_pushvalue(L, -1);
        lua_setuservalue(L, -3);
        lua_setmetatable(L, -2);

        if (lua_isstring(L, 1)) {
                lua_pushvalue(L, 1);
                mem_arg = lua_tolstring(L, -1, &mem_len);

                if (mem_arg) {
                        *ut = unibi_from_mem(mem_arg, mem_len);
                } else {
                        *ut = 0;
                }

                lua_pop(L, 1);
        } else {
                *ut = 0;
        }

        if (!*ut) {
                lua_pop(L, 1);
                lua_pushnil(L);
        }

        return 1;
}

static int
wrap_from_term(lua_State *L)
{
        unibi_term **ut = (unibi_term **) lua_newuserdata(L, sizeof *ut);
        const char *term_arg = 0;

        luaL_getmetatable(L, WRAP_UNIBI_NAME);
        lua_pushvalue(L, -1);
        lua_setuservalue(L, -3);
        lua_setmetatable(L, -2);

        if (lua_isstring(L, 1)) {
                lua_pushvalue(L, 1);
                term_arg = lua_tolstring(L, -1, 0);

                if (term_arg) {
                        *ut = unibi_from_term(term_arg);
                } else {
                        *ut = 0;
                }

                lua_pop(L, 1);
        } else {
                *ut = 0;
        }

        if (!*ut) {
                lua_pop(L, 1);
                lua_pushnil(L);
        }

        return 1;
}

static int
wrap_from_env(lua_State *L)
{
        unibi_term **ut = (unibi_term **) lua_newuserdata(L, sizeof *ut);

        luaL_getmetatable(L, WRAP_UNIBI_NAME);
        lua_pushvalue(L, -1);
        lua_setuservalue(L, -3);
        lua_setmetatable(L, -2);
        *ut = unibi_from_env();

        if (!*ut) {
                lua_pop(L, 1);
                lua_pushnil(L);
        }

        return 1;
}

static const struct luaL_Reg mapping[] = {
        /* */
        { "from_mem", wrap_from_mem },   /* */
        { "from_term", wrap_from_term }, /* */
        { "from_env", wrap_from_env },   /* */
        { 0 },                           /* */
};

/* Lua metatable initialization */
int
luaopen_unibilium (lua_State *L)
{
        lua_newtable(L);
        luaL_setfuncs(L, mapping, 0);
        lua_pushvalue(L, -1);
        lua_pushliteral(L,
                        "Copyright (C) 2008, 2010-2013 Lukas Mai <l DOT mai AT web DOT de>");
        lua_setfield(L, -2, "_COPYRIGHT");
        lua_pushliteral(L, "Unibilium is a very basic terminfo library");
        lua_setfield(L, -2, "_DESCRIPTION");
        lua_pushliteral(L, "lua-unibilium 2.0.0");
        lua_setfield(L, -2, "_VERSION");
        luaL_newmetatable(L, WRAP_UNIBI_NAME);
        luaL_setfuncs(L, wrap_unibi_meta, 0);
        lua_pop(L, 1);
        luaL_newmetatable(L, WRAP_UNIBICMD_NAME);
        luaL_setfuncs(L, wrap_unibicmd_meta, 0);
        lua_pop(L, 1);

        return 1;
}

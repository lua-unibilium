.SUFFIXES:
.SUFFIXES: .o .c

PKG_CONFIG ?= pkg-config
CFLAGS ?= $(shell $(PKG_CONFIG) --cflags lua)
LDFLAGS ?= $(shell $(PKG_CONFIG) --libs lua)

CFLAGS += -Wall -Werror -Wextra

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man
INSTALL_CMOD ?= $(shell $(PKG_CONFIG) --variable INSTALL_CMOD lua)
INSTALL_LMOD ?= $(shell $(PKG_CONFIG) --variable INSTALL_LMOD lua)

CFLAGS  += -fPIC -shared $(shell $(PKG_CONFIG) --cflags unibilium)
LDFLAGS +=               $(shell $(PKG_CONFIG) --libs   unibilium)

CC ?= gcc
LD ?= gcc

.PHONY: all clean install

all: unibilium.so

clean:
	rm unibilium.so wrap-unibilium.o

install: unibilium.so
	mkdir -p $(DESTDIR)$(INSTALL_CMOD)
	cp unibilium.so $(DESTDIR)$(INSTALL_CMOD)

%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $^

unibilium.so: wrap-unibilium.o
	$(CC) -o $@ $^ $(LDFLAGS) -shared -fPIC

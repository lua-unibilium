local unibilium = require("unibilium")

local ut = unibilium.from_env()
if not ut then
        print("Cannot initialize unibilium")
        return
end

local nop = function() return "" end

local cup = ut.cursor_address or nop
local cls = ut.clear or nop
local bell = ut.bell or nop
local sgr = ut.sgr or nop
local enter_underline = ut.enter_underline_mode or nop
local enter_bold = ut.enter_bold_mode or nop
local exit_bold = ut.exit_bold_mode or nop
local enter_reverse = ut.enter_reverse_mode or nop
local setaf = ut.setaf or nop
local setab = ut.setab or nop
local sgr0 = ut.sgr0 or nop

io.write(cls() .. cup(2,5) .. 'Hello ' .. setaf(3) .. setab(6) .. 'World!' .. sgr0())
io.write(cup(3,6) .. 'H' .. enter_underline() .. 'el' .. enter_bold() .. 'lo Wo' .. enter_reverse() .. 'r' .. exit_bold() .. 'ld!' .. sgr0())
io.write(bell())
io.write(cup(ut.lines, 0) .. 'Done now\n')
io.flush()
